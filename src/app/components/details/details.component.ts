import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { Establishment } from '../../models/establishment';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  public id: number = -1;
  public establishment: any;

  constructor(private activatedRoute: ActivatedRoute, private api: ApiService) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    if (id) {
      this.id = parseInt(id);
      if (this.id > -1) {
        this.api.getEstablishments().subscribe(data => {
          this.establishment = data[this.id];
        });
      }
    }
  }

  getFormattedDate(str: string) {
    const pattern = /(\d{2})\-(\d{2})\-(\d{4})/;
    return new Date(str.replace(pattern, '$3-$2-$1'));
  }

}
