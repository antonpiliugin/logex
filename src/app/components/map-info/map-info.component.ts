import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-map-info',
  templateUrl: './map-info.component.html',
  styleUrls: ['./map-info.component.css']
})
export class MapInfoComponent implements OnInit {
  @Input() title: string = '';
  @Input() city: string = '';
  @Input() address: string = '';
  @Input() zip: string = '';
  @Input() url: string = '';
  @Input() year: string = '';
  
  constructor() { }

  ngOnInit(): void {
  }

}
