import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Establishment } from '../../models/establishment';
import { Event } from '../../models/event';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  public showTable: boolean = true;
  private establishments: Establishment[] = [];
  public filteredEstablishments: Establishment[] = [];
  public events: Event[] = [];
  public cities: string[] = [];
  public selectedCities: string[] = [];
  public startYears: string[] = [];
  public selectedYear: string = 'ALL';
  public dropdownSettings: IDropdownSettings = {};
  public page: number = 1;
  public filterByName: string = '';
  public filterByZip: string = '';
  @ViewChild(MapInfoWindow, { static: false }) mapInfoWindow!: MapInfoWindow;
  public mapInfoDetails: any = { title: '', year: '', city: '', address: '', zip: '', url: ''};

  constructor(private api: ApiService) {}

  ngOnInit(): void {
    this.api.getEstablishments().subscribe(data => {
      this.establishments = data;
      this.filteredEstablishments = data;
      this.extractCities(data);
      this.extractStartYears(data);
      console.log(this.establishments);
    });
    this.api.getEvents().subscribe(data => {
      this.events = data;
      console.log(this.events);
    });
  }

  extractCities(est: Establishment[]): void {
    this.cities = est.map(item => item.location.city)
      .filter((value, index, self) => self.indexOf(value) === index);
    this.selectedCities = this.cities;
    this.dropdownSettings = {
      singleSelection: false,
      enableCheckAll: false,
      itemsShowLimit: 2,
      allowSearchFilter: true
    };
    console.log(this.cities);
  }

  extractStartYears(est: Establishment[]): void {
    this.startYears = est.map((item) => {
      if (item.dates.startdate) {
        const d = this.getFormattedDate(item.dates.startdate);
        return d.getFullYear() + '';
      } else {
        return 'UNKNOWN';
      }
    }).filter((value, index, self) => self.indexOf(value) === index).sort((a, b) => {
      return +a - +b;
    });
    this.startYears.unshift('ALL');
  }

  getMapDetails(est: Establishment): any {
    return {
      title: est.title,
      year: est.dates.startdate ? this.getFormattedDate(est.dates.startdate).getFullYear() : 'UNKNOWN',
      city: est.location.city,
      address: est.location.adress,
      zip: est.location.zipcode,
      url: est.urls && est.urls.length ? est.urls[0] : ''
    };
  }

  toggleView(): void {
    this.showTable = !this.showTable;
  }

  getFormattedDate(str: string): any {
    const pattern = /(\d{2})\-(\d{2})\-(\d{4})/;
    return new Date(str.replace(pattern, '$3-$2-$1'));
  }

  getGeoCoords(lat: string, lon: string): google.maps.LatLng {
    return new google.maps.LatLng(parseFloat(lat.replace(',', '.')), parseFloat(lon.replace(',', '.')));
  }

  openMapInfo(marker: MapMarker, est: Establishment): void {
    this.mapInfoDetails = this.getMapDetails(est);
    this.mapInfoWindow.open(marker);
  }

  doFilter(): void {
    this.filteredEstablishments = this.establishments.filter((item) => {
      let doReturn = true;
      // Filter by name
      if (this.filterByName.length && !item.title.toLocaleLowerCase().includes(this.filterByName.toLocaleLowerCase())) {
        doReturn = false;
      }
      // Filter by city
      if (doReturn && this.selectedCities.indexOf(item.location.city) === -1) {
        doReturn = false;
      }
      
      // Filter by postcode
      if (doReturn && this.filterByZip.length && !item.location.zipcode.toLocaleLowerCase().includes(this.filterByZip.toLocaleLowerCase())) {
        doReturn = false;
      }
      // Filter by start year
      if (doReturn && this.selectedYear !== 'ALL') {
        if (item.dates.startdate) {
          const d = this.getFormattedDate(item.dates.startdate);
          const year = d.getFullYear() + '';
          if (year !== this.selectedYear) {
            doReturn = false;
          }
        } else if (this.selectedYear !== 'UNKNOWN') {
          doReturn = false;
        }
      }
      return doReturn;
   });
    
  }

}
