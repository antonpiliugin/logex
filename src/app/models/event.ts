export interface Event {
  trcid: string;
  title: string;
  details: Details;
  types?: Type[];
  location: Location;
  urls?: string[];
  media?: Medum[];
  dates: Dates;
  lastupdated: string;
  eigenschappen?: any;
}

export interface Details {
  de: Language;
  en: Language;
  fr: Language;
  nl: Language;
  it: Language;
  es: Language;
}

export interface Language {
  language: string;
  title: string;
  calendarsummary: string;
  shortdescription: string;
  longdescription: string;
}

export interface Type {
  type: string;
  catid: string;
}

export interface Location {
  name: string;
  city: string;
  adress: string;
  zipcode: string;
  latitude: string;
  longitude: string;
}

export interface Medum {
  url: string;
  main: string;
}

export interface Dates {
  singles?: string[];
  startdate?: string;
  enddate?: string;
}
