import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Establishment } from '../models/establishment';
import { Event } from '../models/event';

const establishmentUrl = 'assets/establishment-data.json';
const eventsUrl = 'assets/events-data.json';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getEstablishments(): Observable<Establishment[]> {
    return this.http.get<Establishment[]>(establishmentUrl);
  }

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(eventsUrl);
  }
}
